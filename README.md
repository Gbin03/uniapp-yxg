# uniapp-yxg

#### 介绍
基于uniapp做的一个小商城的demo

#### 软件架构
软件架构说明

#### 使用说明
yxg-client是利用uni-app开发的客户端代码目录：
1、用HBuilderX打开客户端代码运行即可，可以分别编译成H5、微信小程序和手机app等多客户端；

yxg-server是服务端代码目录：
1、dtcmsdb4.sql是mysql数据库的脚本，用于建立并生成服务端数据；
2、服务端根目录下执行npm install安装依赖，执行node ./src/app.js启动express服务器，启动之前请先安装mysql数据服务和利用dtcmsdb4.sql建立一个dtcmsdb4的数据库；


#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

